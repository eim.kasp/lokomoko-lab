// Initial global game variables
var padding = 10;
var btnOffsetX = 1100;
var btnOffsetY = 40;
var arr = new Array();
var ctrl = new Array();
var levels = new Array();
var current_level = 0;
var inside_level = 0;
var prev_level = 0;
var depth = 0;

var count = 0;
var LevelNumber = 0;

/* Initial function */
function init() {
    game = new labGame();
    game.constructStage(); // Constructor for main stage view
    // Adding buttons to stage
    game.addButton("Скелет");
    game.addButton("Нервная\nсистема");
    game.addButton("Кровеносная\nсистема");
    game.addButton("Внутренние\nорганы ");

    game.addControlls();
    game.addControlls();
    game.addControlls();
    game.addControlls();
    game.moveBar();
}


function labGame() {
    var stage = new createjs.Stage(document.getElementById("lab"));

    /* Enabling touch events */
    createjs.Touch.enable(stage);

    /* Registering sprite sheet of lion*/
    var data = {
        images: ["img/lion.png"],
        frames: {width: 278, height: 364},
        animations: {blink: [0, 19], stand: [0, 0]}
    };
    var spriteSheet;
    var lionAnimation;

    /* Registering stage elements images */
    var ground = new createjs.Bitmap("img/ground.png");
    var shelve = new createjs.Bitmap("img/shelve.png");
    var boy = new createjs.Bitmap("img/boy.png");
    var xray_machine = new createjs.Bitmap("img/xray_machine.png");
    var top_leaf = new createjs.Bitmap("img/top_leaf.png");
    var top_vitamins = new createjs.Bitmap("img/top_vitamins.png");
    var top_bar = new createjs.Bitmap("img/top_bar.png");
    var button1 = new createjs.Bitmap("img/button1.png");
    button1.id = 1;
    var button2 = new createjs.Bitmap("img/button2.png");
    button2.id = 2;
    var button3 = new createjs.Bitmap("img/button3.png");
    button3.id = 3;
    var skelet = new createjs.Bitmap("img/skelet.png");
    var cont_container = new createjs.Bitmap("img/cont_container.png");
    var indicator = new createjs.Bitmap("img/cursor.png");
    var screenText = new createjs.Text("Скелет – наши косточки соединены между собой и все вместе составляют скелет. Внутри тебя тоже есть скелет, и выглядит он так.");
    var image = new Image();
    screenText.font = "16px Open Sans";
    screenText.color = "#ffffff";
    screenText.x = 390;
    screenText.y = 320;
    screenText.lineWidth = 200;
    screenText.textAlign = "center";
    image.src = "img/skelet.png";
    var actionScene = new createjs.Container();
    actionScene.x = 830;
    actionScene.y = 60;
    actionScene.addEventListener("mouseover", actionHoverHandler);
    actionScene.addEventListener("mouseout", actionOffHandler);
    var drawingCanvas = new createjs.Shape();
    var bitmap = new createjs.Bitmap(image);
    bitmap.scale = 0.5;
    var shape = new createjs.Shape();
    shape.graphics = new createjs.Graphics().beginStroke("rgba(255,255,255,1)").beginBitmapFill(bitmap.image).drawRect(0, 0, 195, 370).endStroke();
    /* Create mask to be sure about big images cases */
    bitmap.mask = shape;
    var blur = new createjs.Bitmap(image);
    bitmap.scaleX = 1;
    bitmap.scaleY = 1;


    blur.filters = [new createjs.BlurFilter(15, 15, 2)];
    blur.cache(0, 0, 960, 400);
    blur.alpha = 0.9;
    actionScene.addChild(bitmap, indicator);
    stage.update();
    // note that the shape can be used in the display list as well if you'd like, or
    // we can reuse the Graphics instance in another shape if we'd like to transform it differently.
    var ctrl_count = 0;
    this.addControlls = function () {
        var container = new createjs.Container();
        if (ctrl_count == 0) {
            var btn = new createjs.Bitmap("img/ctrl_zoomIn.png");
            btn.addEventListener("click", zoomInClick);
        }

        else if (ctrl_count == 1) {
            var btn = new createjs.Bitmap("img/ctrl_zoomOut.png");
            btn.addEventListener("click", zoomOutClick);
        }

        else if (ctrl_count == 2) {
            var btn = new createjs.Bitmap("img/ctrl_up.png");
            btn.addEventListener("click", upClick);
        }

        else if (ctrl_count == 3) {
            var btn = new createjs.Bitmap("img/ctrl_down.png");
            btn.addEventListener("click", downClick);
        }

        else {
            var btn = new createjs.Bitmap("img/controll-button.png");
        }

        container.id = ctrl_count;
        ctrl_count++;
        container.x = 850 + ctrl_count * 30;
        container.y = 435;
        container.addChild(btn);
        stage.addChild(container);
        ctrl.push(container);
        stage.update();
    }

    this.addLevel = function (x, y) {
        var level = new createjs.Shape();
        level.graphics.beginFill("red").drawCircle(0, 0, 0);
        level.x = x;
        level.y = y;
        level.id = LevelNumber;
        actionScene.addChild(level);
        level.addEventListener("click", levelClick);
        levels.push(level);
        LevelNumber++;
        indicator.x = levels[0].x - 50;
        indicator.y = levels[0].y - 40;
    }

    // Dynamically add buttons
    this.addButton = function (text) {

        // Create container object
        var container = new createjs.Container();

        // Assign background image based on scenario
        var overlay = new createjs.Shape();
        overlay.graphics.beginFill("#fff").drawRect(0, 0, 103, 60);
        overlay.alpha = 0.1;
        if (count == 0) {
            var btn = new createjs.Bitmap("img/cta_active.png");
            btn.active = 1;
            overlay.active = 1;

            var label = new createjs.Text(text, "bold 14px Open Sans ", "#ffffff");
            label.x = 53;
            label.y = 22;
        } else {
            var btn = new createjs.Bitmap("img/cta_simple.png");
            var label = new createjs.Text(text, "14px Open Sans", "#6d512c");
            label.x = 53;
            label.y = 13;
            btn.active = 0;
        }

        var hit = new createjs.Shape();
        hit.graphics.beginFill("#000").drawRect(0, 0, 153, 60);
        label.hitArea = hit;

        overlay.id = count;
        label.textAlign = "center";
        overlay.addEventListener("mouseover", hoverHandler);
        overlay.addEventListener("mouseout", leaveHandler);
        overlay.addEventListener("click", pressHandler);
        btn.x = 0;
        btn.y = 0;

        // Centering text @TODO
        b = label.getBounds();
        a = btn.getBounds();

        // Position container position based on count of ellements
        container.x = btnOffsetX;
        container.y = btnOffsetY + (count * 80);
        container.name = "shape" + count;
        container.id = count;
        label.id = count;
        btn.id = count;
        container.type = "Button";
        stage.update();
        count++;

        // Add child elements to container
        container.addChild(btn, label, overlay);

        // Add container to array of elements
        arr.push(container);

        // Add container to stage
        stage.addChild(container);
    }

    // Construct stage
    this.constructStage = function () {
        var canvas = document.getElementById('lab');
        var context = canvas.getContext('2d');
        stage.enableMouseOver();
//        canvas.width = window.innerWidth;
//        canvas.height = window.innerWidth / 2.105;
        constructLevels();
        createjs.Ticker.addEventListener("tick", tick);
        spriteSheet = new createjs.SpriteSheet(data);
        lionAnimation = new createjs.Sprite(spriteSheet, "blink");
        lionAnimation.x = 12;
        lionAnimation.y = 240;
        ground.x = 0;
        ground.y = 450;
        shelve.y = 200;
        shelve.x = 220;
        boy.x = 550;
        boy.y = 75;
        top_leaf.x = 300;
        top_leaf.y = 20;
        top_vitamins.x = 10;
        top_vitamins.y = 20;
        xray_machine.x = 700; // 750
        xray_machine.y = 0;
        top_bar.x = 540;
        top_bar.y = 55;

        /* Positioning buttons to right place */
        button1.x = 775;
        button1.y = 813;
        button2.y = 885;
        button2.x = 920;
        button3.x = 1073;
        button3.y = 870;

        /* Animating buttons on load to slide in */
        createjs.Tween.get(button1, {loop: false})
            .wait(100) // wait for 1 second
            .set({}, button1)
            .to({y: 513}, 1000, createjs.Ease.linear);
        createjs.Tween.get(button2, {loop: false})
            .wait(300) // wait for 1 second
            .to({y: 485}, 1000, createjs.Ease.linear);
        createjs.Tween.get(button3, {loop: false})
            .wait(500) // wait for 1 second
            .to({y: 472}, 1000, createjs.Ease.linear);
        button1.addEventListener("mouseover", buttonHover);
        button2.addEventListener("mouseover", buttonHover);
        button3.addEventListener("mouseover", buttonHover);
        button1.addEventListener("mouseout", buttonLeave);
        button2.addEventListener("mouseout", buttonLeave);
        button3.addEventListener("mouseout", buttonLeave);

        /* Function to move bar over the boy */
        this.moveBar = function () {
            createjs.Tween.get(top_bar, {loop: false})
                .wait(100) // wait for 1 second
                .set({x: 540}, top_bar)
                .to({y: 200}, 2000, createjs.Ease.linear)
                .to({y: 180}, 500, createjs.Ease.linear)
                .to({y: 220}, 500, createjs.Ease.linear)
                .to({y: 200}, 500, createjs.Ease.linear)
                .to({y: 400}, 1000, createjs.Ease.linear)
                .wait(1000)
                .to({y: 55}, 1000, createjs.Ease.linear); // tween to scaleX/Y of 1 with ease bounce out
        }

        /* Add all elements to stage in single call */
        stage.addChild(ground, shelve, lionAnimation, boy, top_vitamins, top_leaf, top_bar, actionScene, xray_machine, button3, button2, button1, screenText);
        stage.update();
    }

    function tick() {
        stage.update();
    }

    function pressHandler(e) {
        for (i = 0; i <= 3; i++) {
            arr[i].children[0].active = 0;
            arr[i].children[0].image.src = "img/cta_simple.png";
            arr[i].children[1].font = "14px Open Sans";
            arr[i].children[1].color = "#6d512c";
            arr[i].children[2].active = 0;
            e.target.atctive = 0;
            arr[e.target.id].children[2].active = 0;
        }
        arr[e.target.id].children[1].font = "bold 14px Open Sans";
        arr[e.target.id].children[1].color = "#ffffff";
        arr[e.target.id].children[2].active = 1;
        e.target.active = 1;
        arr[e.target.id].children[0].image.src = "img/cta_active.png";
    }

    function hoverHandler(e) {
        arr[e.target.id].children[0].image.src =  "img/cta_active.png";
        arr[e.target.id].children[1].font = "bold 14px Open Sans";
        arr[e.target.id].children[1].color = "#ffffff";
        console.log(arr[e.target.id].children[2].active)
    }

    function leaveHandler(e) {
        if (arr[e.target.id].children[2].active != 1) {
            arr[e.target.id].children[0].image.src =  "img/cta_simple.png";
            arr[e.target.id].children[1].font = "14px Open Sans";
            arr[e.target.id].children[1].color = "#6d512c";
        }
    }

    function actionHoverHandler(e) {
        $("#lab").addClass("custom_cursor");
    }

    function actionOffHandler(e) {
        $("#lab").removeClass("custom_cursor");
    }

    function levelClick(e) {
        current_level = e.target.id;
    }

    function upClick(e) {
        if (depth == 0) {
            if (current_level > 0) {
                current_level--;
                moveIndicator(current_level);
            }
        }
        else {
            if (inside_level > 0) {
                inside_level--;
                moveIndicator(inside_level);
            }
        }

    }

    function downClick(e) {

        if (depth == 0) {
            if(current_level < LevelNumber)
            {
                current_level++;
                moveIndicator(current_level);
            }
        }
        else {
            if(current_level < LevelNumber){
                inside_level++;
                moveIndicator(inside_level);
            }


        }
    }

    function zoomInClick(e) {

        if(LevelNumber > 0)
        {
            game.moveBar();
            depth++;
            inside_level = 0;
            moveIndicator(0);
            destroyLevels();
        }

        if (depth == 1) {
            bitmap.image.src = "img/skelet_" + current_level + "_" + depth + ".png";
            constructLevels();
        }
        if (depth == 2) {
            bitmap.image.src = "img/skelet_" + current_level + "_" + depth + "_" + inside_level + ".png";
            constructLevels();
        }
    }

    /* Creating cases and adding cordinates for levels */
    function constructLevels() {

        if (current_level == 1 && depth == 1) {
            game.addLevel(110, 100);
        }

        if (depth == 0) {
            game.addLevel(110, 70);
            game.addLevel(110, 150);
            game.addLevel(110, 320);
        }

        if (LevelNumber == 0) {
            indicator.alpha = 0;
        } else {
            indicator.alpha = 1;
        }
    }

    /* Zoom out click event for interacting with scene */
    function zoomOutClick(e) {

        if (depth > 0) {
            depth--;
            destroyLevels();
        }

        if (depth == 0) {
            bitmap.image.src = "img/skelet.png";
            current_level = 0;
            constructLevels();
            moveIndicator(0);

        } else {
            bitmap.image.src = "img/skelet_" + current_level + "_" + depth + ".png";
            constructLevels();
            moveIndicator(0);
        }
    }

    /* Move skelet indicator over the boy */
    function moveIndicator(level) {
        indicator.x = levels[level].x - 50;
        indicator.y = levels[level].y - 40;
    }

    function destroyLevels() {
        for (i = 0; i <= LevelNumber; i++) {
            actionScene.removeChild(levels[i]);
        }
        LevelNumber = 0;
        levels = [];
    }

    /* Button hover event */
    function buttonHover(e) {

        /*  Button hover highlight effect */
        if(e.target.id == 1) {
            createjs.Tween.get(button2, {loop: false})
                .to({y: 550}, 200, createjs.Ease.linear);
            createjs.Tween.get(button3, {loop: false})
                .to({y: 550}, 200, createjs.Ease.linear);
        }

        if(e.target.id == 2) {
            createjs.Tween.get(button1, {loop: false})
                .to({y: 550}, 200, createjs.Ease.linear);
            createjs.Tween.get(button3, {loop: false})
                .to({y: 550}, 200, createjs.Ease.linear);
        }

        if(e.target.id == 3) {
            createjs.Tween.get(button1, {loop: false})
                .to({y: 550}, 200, createjs.Ease.linear);
            createjs.Tween.get(button2, {loop: false})
                .to({y: 550}, 200, createjs.Ease.linear);
        }
    }

    function buttonLeave(e) {

        /* Put buttons to original possition */
        if(e.target.id == 1) {
            createjs.Tween.get(button2, {loop: false})
                .to({y: 485}, 200, createjs.Ease.linear);
            createjs.Tween.get(button3, {loop: false})
                .to({y: 472}, 200, createjs.Ease.linear);
        }

        if(e.target.id == 2) {
            createjs.Tween.get(button1, {loop: false})
                .to({y: 513}, 200, createjs.Ease.linear);
        createjs.Tween.get(button3, {loop: false})
            .to({y: 472}, 200, createjs.Ease.linear);
        }

        if(e.target.id == 3) {
            createjs.Tween.get(button1, {loop: false})
                .to({y: 513}, 200, createjs.Ease.linear);
            createjs.Tween.get(button2, {loop: false})
                .to({y: 485}, 200, createjs.Ease.linear);
        }
    }

    /* Keyboard detection */
    document.onkeydown = checkKey;

    function checkKey(e) {

        e = e || window.event;

        if (e.keyCode == '8' || e.keyCode == '27') {
            zoomOutClick(e);
        }

        if (e.keyCode == '13' || e.keyCode == '32') {
            zoomInClick(e);
        }

        else if (e.keyCode == '38') {
            upClick(e);
        }

        else if (e.keyCode == '40') {
            downClick(e);
        }
    }

    /*************************/
    /*Unused functions */
    function lion_stand() {
        lionAnimation = new createjs.Sprite(spriteSheet, "stand");
    }

    function lion_talk() {
        lionAnimation = new createjs.Sprite(spriteSheet, "blink");
    }

    function after_load() {

    }

    var generateSprites = function () {
        lionAnimation.x = 1030;
        lionAnimation.y = 450;
        lionAnimation.currentFrame = 0;
    }
}


